#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include <sys/wait.h>

const char *FILE_NAME = "file.txt";

void childSignalHandler(int signal) {
    FILE *f = fopen(FILE_NAME, "a");
    fwrite("2", 1, 1, f);
    fclose(f);
    if (signal == SIGCONT) {
        raise(SIGUSR1);
    } else {
        raise(SIGSTOP); // Приостановить потомка, иначе он завершит свою работу прежде,
    }                   // чем родитель доделает свою работу
}

int main() {
    printf("Сколько раз повторить последовательность? Введите натуральное число: ");
    int n;
    scanf("%d", &n);

    FILE *f = fopen(FILE_NAME, "w");
    fclose(f);

    int pid = fork();
    if (pid < 0) {
        printf("error");
        return -1;
    }

    if (pid == 0) {
        signal(SIGCONT, childSignalHandler); //Обработчики сигналов
        signal(SIGUSR1, childSignalHandler);
        raise(SIGSTOP);                      //Остановиться после устновки обработчиков
    } else {
        for (int i = 0; i < n; i++) {
            waitpid(pid, NULL, WUNTRACED); // Ожидание, пока приостановится потомок
            f = fopen(FILE_NAME, "a");
            fwrite("1", 1, 1, f);
            fclose(f);
            kill(pid, SIGCONT);  // Возобновить потомка
        }
        waitpid(pid, NULL, WUNTRACED); // Дождаться остановки потомка
        kill(pid, SIGKILL);            // Убить потомка
    }
    return 0;
}
